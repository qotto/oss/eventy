# Copyright (c) Qotto, 2021

"""
Messaging errors
"""

__all__ = [
    'MessagingError',
]


class MessagingError(Exception):
    """
    Base Error class of all messaging API
    """
