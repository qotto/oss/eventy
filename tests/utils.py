# Copyright (c) Qotto, 2021
from functools import wraps
from typing import Callable

from eventy.trace_id import request_id_var, correlation_id_var


def process(**kwargs):
    kwargs.update(process_request_id=request_id_var.get())
    kwargs.update(process_correlation_id=correlation_id_var.get())
    return kwargs


def noop(name='noop_name', info='noop_info') -> Callable:
    def decorator(func):
        def decorated(*args, **kwargs):
            res = func(*args, **kwargs)
            res.update(decorated_with_name=name)
            res.update(decorated_with_info=info)
            return res

        return decorated

    return decorator


def mock_trace_id_generator(trace: str):
    def gen_trace_id(func: Callable):
        """
        Mocked trace_id generation
        """
        return trace

    return gen_trace_id


def mock_celery_send_task(name, args, kwargs):
    """
    Mocked send_task that does nothing and return input kwargs
    """
    return kwargs


def mock_celery_shared_task(_func=None, **kwargs):
    """
    Decorated function will get all kwargs of decorator prefixed by 'decorated_' and
    2 kwargs : task_correlation_id and task_request_id with current traces in context
    """

    def shared_task_decorator(function: Callable):
        @wraps(function)
        def decorated_shared_task(*func_args, **func_kwargs):
            for k, v in kwargs.items():
                func_kwargs.update({f'decorated_{k}': v})
            return function(*func_args, **func_kwargs)

        return decorated_shared_task

    return shared_task_decorator
