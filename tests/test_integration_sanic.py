# Copyright (c) Qotto, 2021
import logging
import unittest
from io import StringIO

from sanic import json
from sanic_testing import TestManager

from eventy import config
from eventy.integration.sanic import Sanic
from eventy.logging import SimpleHandler

logger = logging.getLogger(__name__)


class TestIntegrationSanic(unittest.TestCase):
    """
    Test eventy.integration.sanic module
    """

    def setUp(self) -> None:
        config.SERVICE_NAME = 'test-integration-sanic'

        self.log_string_io = StringIO()
        self.log_handler = SimpleHandler(colored=False, level='INFO')
        self.log_handler.stream = self.log_string_io
        logging.getLogger().addHandler(self.log_handler)

        app = Sanic('Test_Sanic_App')
        TestManager(app)

        @app.route('/hello')
        async def test_hello(request):
            logger.info("Hello")
            return json({'hello': 'world'})

        @app.route('/raise-error')
        async def test_raise(request):
            logger.info("Raise error")
            raise RuntimeError('My Error')

        @app.route('/health')
        async def test_health(request):
            logger.info("Health")
            return json({'health': 'OK'})

        self.app: Sanic = app

    def test_sanic_access_logs(self):
        request, response = self.app.test_client.get('/hello')
        self.assertEqual(response.status, 200)

        self.assertRegex(
            self.log_string_io.getvalue(),
            'INFO.*GET.*hello.*200'
        )

    def test_correlation_id_provider(self):
        request, response = self.app.test_client.get('/hello', headers={'x-correlation-id': 'my_correlation_id'})
        self.assertEqual(response.status, 200)

        self.assertRegex(
            self.log_string_io.getvalue(),
            'CID:my_correlation_id'
        )
