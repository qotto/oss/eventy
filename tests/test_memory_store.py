# Copyright (c) Qotto, 2021
import os
import unittest

from eventy.messaging.memory import MemoryStore
from eventy.record import Event
from eventy.serialization.avro import AvroSerializer


def create_event(a_text: str, an_integer: int):
    return Event(
        protocol_version="3.0.0",
        namespace='urn:domain:service',
        name='TestEvent',
        version='1.0.0',
        source='urn:domain:service',
        data={
            'a_text': a_text,
            'an_integer': an_integer,
        }
    )


class TestMemoryStore(unittest.TestCase):
    """
    Test MemoryStore
    """

    def test_write_record(self):
        store = MemoryStore()
        store.write(create_event('a', 1), 'topic')
        store.write(create_event('b', 2), 'topic')
        store.write(create_event('b', 2), 'other-topic')
        self.assertEqual(len(store.get(committed=True)), 3)
        self.assertEqual(store.get_committed_records('topic')[0].data['a_text'], 'a')
        self.assertEqual(store.get_committed_records('topic')[1].data['a_text'], 'b')
        self.assertEqual(store.get_committed_records('other-topic')[0].data['a_text'], 'b')

    def test_write_record_with_avro(self):
        store = MemoryStore(AvroSerializer.from_schemas_folder(os.path.abspath('tests/avro_schemas')))
        store.write(create_event('a', 1), 'topic')
        store.write(create_event('b', 2), 'topic')
        store.write(create_event('b', 2), 'other-topic')
        self.assertEqual(len(store.get(committed=True)), 3)
        self.assertEqual(store.get_committed_records('topic')[0].data['a_text'], 'a')
        self.assertEqual(store.get_committed_records('topic')[1].data['a_text'], 'b')
        self.assertEqual(store.get_committed_records('other-topic')[0].data['a_text'], 'b')

    def test_write_record_in_transaction(self):
        store = MemoryStore()
        store.start_transaction()
        store.write(create_event('a', 1), 'topic')
        store.write(create_event('b', 2), 'topic')
        self.assertEqual(len(store.get_committed_records()), 0)
        store.commit()
        self.assertEqual(len(store.get(committed=True)), 2)
        self.assertEqual(store.get_committed_records()[0].data['a_text'], 'a')
        self.assertEqual(store.get_committed_records()[1].data['a_text'], 'b')

    def test_write_record_abort_transaction(self):
        store = MemoryStore()
        store.start_transaction()
        store.write(create_event('a', 1), 'topic')
        store.write(create_event('b', 2), 'topic')
        self.assertEqual(len(store.get_committed_records()), 0)
        self.assertEqual(len(store.get_all_written_records()), 2)
        store.abort()
        self.assertEqual(len(store.get_all_written_records()), 0)

        store.start_transaction()
        store.write(create_event('a', 1), 'topic')
        store.write(create_event('b', 2), 'topic')
        store.commit()
        self.assertEqual(len(store.get_committed_records()), 2)
        self.assertEqual(store.get_committed_records()[0].data['a_text'], 'a')
        self.assertEqual(store.get_committed_records()[1].data['a_text'], 'b')

    def test_read_write_in_transaction(self):
        store = MemoryStore()
        store.register_topic('topic')
        store.add_record(create_event('a', 1), 'topic')
        store.start_transaction()
        event = store.read()[0]
        store.ack()
        store.write(event, 'other-topic')
        self.assertEqual(event.data['a_text'], 'a')
        self.assertEqual(len(store.get_committed_records()), 0)
        self.assertEqual(len(store.get_acked_records()), 0)
        self.assertEqual(len(store.get_all_written_records()), 1)
        self.assertEqual(len(store.get_all_read_records()), 1)
        store.commit()
        self.assertEqual(len(store.get_committed_records()), 1)
        self.assertEqual(len(store.get_acked_records()), 1)
