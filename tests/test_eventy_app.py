# Copyright (c) Qotto, 2021
import unittest
from time import sleep
from typing import Iterable

from eventy.messaging import EventyApp, Service, handle_event, Guarantee, Agent
from eventy.messaging.memory import MemoryStore
from eventy.record import Event, Record

TIMEOUT_MS = 1000

def create_my_event(a_text: str, an_integer: int):
    return Event(
        protocol_version="3.0.0",
        namespace='urn:my-service',
        name='MyEvent',
        version='1.0.0',
        source='urn:my-service',
        data={
            'a_text': a_text,
            'an_integer': an_integer,
        }
    )


def create_other_event(a_text: str, an_integer: int):
    return Event(
        protocol_version="3.0.0",
        namespace='urn:other-service',
        name='OtherEvent',
        version='1.0.0',
        source='urn:other-service',
        data={
            'a_text': a_text,
            'an_integer': an_integer,
        }
    )


def handle_other_event(other_event: Event) -> Iterable[Record]:
    yield create_my_event(other_event.data['a_text'] + ' processed', 0)


def handle_other_event_no_output(other_event: Event) -> Iterable[Record]:
    ...


class MyAgent(Agent):
    @handle_event('other-service', 'OtherEvent', Guarantee.AT_LEAST_ONCE)
    def handle_other_event(self, other_event: Event) -> Iterable[Record]:
        yield create_my_event(other_event.data['a_text'] + ' processed', 0)

    @handle_event('other-service', 'OtherEvent', Guarantee.AT_LEAST_ONCE)
    def handle_other_event_no_output(self, other_event: Event) -> Iterable[Record]:
        ...


class TestEventyApp(unittest.TestCase):
    """
    Test Eventy App.
    """

    def test_simple(self):
        record_store = MemoryStore()
        eventy_app = EventyApp(
            record_store, Service('my-service'), [Service('other-service')],
            handlers=[handle_event('other-service', 'OtherEvent', Guarantee.EXACTLY_ONCE)(handle_other_event)],
        )

        eventy_app.run()

        record_store.add_record(create_other_event('hello', 1), 'other-service-events')
        record_store.add_record(create_other_event('hello again', 2), 'other-service-events')

        record_store.wait_all_acked(timeout_ms=TIMEOUT_MS)
        eventy_app.cancel()

        self.assertEqual(len(record_store.get('my-service-events', committed=True)), 2)

    def test_no_output(self):
        record_store = MemoryStore()
        eventy_app = EventyApp(
            record_store, Service('my-service'), [Service('other-service')],
            handlers=[
                handle_event('other-service', 'OtherEvent', Guarantee.EXACTLY_ONCE)(handle_other_event_no_output)],
        )

        eventy_app.run()

        record_store.add_record(create_other_event('hello', 1), 'other-service-events')
        record_store.add_record(create_other_event('hello again', 2), 'other-service-events')

        record_store.wait_all_acked(timeout_ms=TIMEOUT_MS)
        eventy_app.cancel()

        self.assertEqual(len(record_store.get('other-service-events', acked=True)), 2)


class TestEventyAppWithAgent(unittest.TestCase):
    """
    Test Eventy App.
    """

    def test_simple(self):
        record_store = MemoryStore()
        eventy_app = EventyApp(
            record_store, Service('my-service'), [Service('other-service')],
            agents=[MyAgent()],
        )

        eventy_app.run()

        record_store.add_record(create_other_event('hello', 1), 'other-service-events')
        record_store.add_record(create_other_event('hello again', 2), 'other-service-events')

        record_store.wait_all_acked(timeout_ms=TIMEOUT_MS)
        eventy_app.cancel()

        self.assertEqual(len(record_store.get('my-service-events', committed=True)), 2)

    def test_no_output(self):
        record_store = MemoryStore()
        eventy_app = EventyApp(
            record_store, Service('my-service'), [Service('other-service')],
            agents=[MyAgent()],
        )

        eventy_app.run()

        record_store.add_record(create_other_event('hello', 1), 'other-service-events')
        record_store.add_record(create_other_event('hello again', 2), 'other-service-events')

        record_store.wait_all_acked(timeout_ms=TIMEOUT_MS)
        eventy_app.cancel()

        self.assertEqual(len(record_store.get('other-service-events', acked=True)), 2)

    def test_call_agent(self):
        result = list(MyAgent().handle_other_event(create_other_event('hello', 1)))
        self.assertEqual(len(result), 1)
