# Copyright (c) Qotto, 2021

import logging
import unittest
from unittest import mock, skip

from eventy import config
from eventy.integration.celery import shared_task, send_task
from eventy.trace_id import correlation_id_var, request_id_var
from tests.utils import mock_trace_id_generator, mock_celery_shared_task, mock_celery_send_task

logger = logging.getLogger(__name__)


class TestIntegrationCelery(unittest.TestCase):
    """
    Test eventy.integration.celery module
    """

    def setUp(self) -> None:
        config.SERVICE_NAME = 'test'

    @mock.patch('eventy.integration.celery.gen_trace_id', new=mock_trace_id_generator('generated-trace_id'))
    @mock.patch('eventy.integration.celery.celery_shared_task', new=mock_celery_shared_task)
    def test_correlation_id_extraction_from_shared_task(self):
        @shared_task
        def noop_celery_task(**kwargs):
            kwargs.update(task_correlation_id=correlation_id_var.get())
            kwargs.update(task_request_id=request_id_var.get())
            return kwargs

        self.assertEqual(
            noop_celery_task(param='value'),
            {
                'param': 'value',
                'task_correlation_id': 'generated-trace_id',
                'task_request_id': 'generated-trace_id',
            },
        )
        self.assertEqual(
            noop_celery_task(param='value', correlation_id='test-correlation_id'),
            {
                'param': 'value',
                'task_correlation_id': 'test-correlation_id',
                'task_request_id': 'generated-trace_id',
            },
        )

    @mock.patch('eventy.integration.celery.gen_trace_id', new=mock_trace_id_generator('generated-trace_id'))
    @mock.patch('eventy.integration.celery.celery_shared_task', new=mock_celery_shared_task)
    def test_shared_task_decorated_param(self):

        @shared_task(param='decorated_value')
        def decorated_celery_task(**kwargs):
            kwargs.update(task_correlation_id=correlation_id_var.get())
            kwargs.update(task_request_id=request_id_var.get())
            return kwargs

        self.assertEqual(
            decorated_celery_task(param='value'),
            {
                'param': 'value',  # function param
                'decorated_param': 'decorated_value',  # decorator param
                'task_correlation_id': 'generated-trace_id',
                'task_request_id': 'generated-trace_id',
            },
        )

    @mock.patch('eventy.integration.celery.celery_send_task', new=mock_celery_send_task)
    def test_send_task(self):
        self.assertEqual(
            send_task('my.task.name', [], {'param': 'value'}),
            {
                'param': 'value',
            },
        )

        # Try block so that context is not set for other tests in case of failure
        token = correlation_id_var.set('my_correlation')
        try:
            self.assertEqual(
                send_task('my.task.name', [], {'param': 'value'}),
                {
                    'param': 'value',
                    'correlation_id': 'my_correlation',
                },
            )
        except Exception:
            raise
        finally:
            correlation_id_var.reset(token)
