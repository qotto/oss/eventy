# Copyright (c) Qotto, 2021

import unittest

import eventy.config
from eventy.integration.celery import shared_task
from eventy.trace_id.generator import gen_trace_id


class TestTraceId(unittest.TestCase):
    """
    Test of trace_id API
    """

    def setUp(self) -> None:
        eventy.config.SERVICE_NAME = 'my_service'

    def test_gen_trace_id_func(self):
        def my_function():
            pass

        trace = gen_trace_id(my_function)
        self.assertRegex(trace, 'my_service:my_function:.*')

    def test_gen_trace_id_str(self):
        trace = gen_trace_id('my_text')
        self.assertRegex(trace, 'my_service:my_text:.*')

    def test_gen_trace_id_obj(self):
        trace = gen_trace_id(4)
        self.assertRegex(trace, 'my_service:4:.*')

    def test_gen_trace_id_on_decorated_function(self):
        @shared_task
        def my_function():
            pass

        @shared_task(param='value')
        def my_function_decorator_param():
            pass

        trace = gen_trace_id(my_function)
        self.assertRegex(trace, 'my_service:my_function:.*')
        trace = gen_trace_id(my_function_decorator_param)
        self.assertRegex(trace, 'my_service:my_function_decorator_param:.*')
