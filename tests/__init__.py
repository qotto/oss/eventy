# Copyright (c) Qotto, 2021
import logging

from eventy.logging import SimpleHandler

logging.getLogger().setLevel('DEBUG')
logging.getLogger().addHandler(SimpleHandler(colored=True))
