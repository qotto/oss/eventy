# Copyright (c) Qotto, 2021

from __future__ import annotations

import os
from datetime import datetime

import pytest
import yaml

from eventy.record import Event
from eventy.serialization.avro import AvroSerializer, _decode_avro_record_legacy, AvroDecodeError


@pytest.fixture()
def avro_serializer():
    return AvroSerializer.from_schemas_folder(os.path.abspath('tests/avro_schemas'))


class TestAvroSerializerProtocolV3:
    @pytest.fixture()
    def create_event(self):
        def _create_event(
            namespace='urn:domain:service',
            source='urn:domain:service',
            a_text='a_text',
            an_integer=13,
        ):
            return Event(
                protocol_version="3.0.0",
                namespace=namespace,
                name='TestEvent',
                version='1.0.0',
                source=source,
                correlation_id='test_service:cid:123',
                context={
                    'urn:domain:service': {
                        'foo': 'bar'
                    }
                },
                data={
                    'a_text': a_text,
                    'an_integer': an_integer,
                }
            )

        return _create_event

    @pytest.mark.parametrize("namespace", ['urn:domain:service', 'urn:domain:service-dash', 'urn:domain:service.dot', ])
    def test_namespace(self, namespace, create_event, avro_serializer):
        event = create_event(namespace)
        encoded = avro_serializer.encode(event)
        decoded = avro_serializer.decode(encoded)
        assert decoded.namespace == namespace

    @pytest.mark.parametrize("source", ['urn:domain:service', 'urn:domain:service-dash', 'urn:domain:service.dot', ])
    @pytest.mark.parametrize("a_text", ['', 'Simple text', ])
    @pytest.mark.parametrize("an_integer", [0, 1, -1, ])
    def test_data(self, create_event, source, a_text, an_integer, avro_serializer):
        event = create_event(source=source, a_text=a_text, an_integer=an_integer)
        encoded = avro_serializer.encode(event)
        decoded = avro_serializer.decode(encoded)
        assert decoded.source == source
        assert decoded.data['a_text'] == a_text
        assert decoded.data['an_integer'] == an_integer


class TestAvroSerializerProtocolV1Legacy:

    @pytest.fixture()
    def create_schema_data_dict_legacy(self):
        def _create_schema_data_dict_legacy(namespace):
            schema_yml = f"""---
    namespace: {namespace}
    name: ClientCreated
    type: record
    fields:
    # general fields
    - name: correlation_id
      type:
        - "null"
        - string
    - name: event_timestamp
      doc: UNIX timestamp, in milliseconds
      type:
        type: long
        logicalType: timestamp-millis
    - name: source
      doc: event source
      type:
        type: string
    # specific fields
    - name: client_id
      type: long
    - name: name
      type:
        - "null"
        - string
            """
            return yaml.load(schema_yml, Loader=yaml.SafeLoader)

        return _create_schema_data_dict_legacy

    @pytest.fixture()
    def create_event_data_dict_legacy(self):
        def _create_event_data_dict_legacy(source, client_id, name):
            date = datetime.fromisoformat('2021-01-01T01:01:01.001+00:00')
            return {
                'correlation_id': 'cid',
                'event_timestamp': int(date.timestamp() * 1000),
                'client_id': client_id,
                'source': source,
                'name': name,
            }

        return _create_event_data_dict_legacy

    @pytest.mark.parametrize(
        "namespace, expected_namespace",
        [
            ('service', 'urn:service'),
            ('service:colon.dot-dash', 'urn:service:colon:dot-dash'),
        ]
    )
    @pytest.mark.parametrize(
        "source, expected_source", [
            ('service', 'urn:service'),
            ('service:colon.dot-dash', 'urn:service:colon:dot-dash'),
        ]
    )
    @pytest.mark.parametrize("client_id", [0, 1, ])
    @pytest.mark.parametrize("name", [None, '', 'Mrs Client', ])
    def test_legacy(
        self,
        create_schema_data_dict_legacy, create_event_data_dict_legacy,
        namespace, expected_namespace,
        source, expected_source,
        client_id, name
    ):
        event = _decode_avro_record_legacy(
            create_schema_data_dict_legacy(namespace),
            create_event_data_dict_legacy(source, client_id, name),
        )
        assert event.namespace == expected_namespace
        assert event.source == expected_source
        assert event.data.get('client_id') == client_id
        assert event.data.get('name') == name


class TestAvroSerializerProtocolV1LegacyBis:

    @pytest.fixture()
    def create_schema_data_dict_legacy(self):
        def _create_schema_data_dict_legacy(namespace):
            schema_yml = f"""---
namespace: {namespace}
name: ClientCreated
type: record
fields:
# general fields
- name: correlation_id
  type:
    - "null"
    - string
- name: event_timestamp
  doc: UNIX timestamp, in milliseconds
  type:
    type: long
    logicalType: timestamp-millis
# specific fields
- name: client_id
  type: long
- name: name
  type:
    - "null"
    - string
        """
            return yaml.load(schema_yml, Loader=yaml.SafeLoader)

        return _create_schema_data_dict_legacy

    @pytest.fixture()
    def create_event_data_dict_legacy(self):
        def _create_event_data_dict_legacy(client_id, name):
            date = datetime.fromisoformat('2021-01-01T01:01:01.001+00:00')
            return {
                'correlation_id': 'cid',
                'event_timestamp': int(date.timestamp() * 1000),
                'client_id': client_id,
                'name': name,
            }

        return _create_event_data_dict_legacy

    @pytest.mark.parametrize(
        "namespace, expected_namespace",
        [
            ('service', 'urn:service'),
            ('service:colon.dot-dash', 'urn:service:colon:dot-dash'),
        ]
    )
    @pytest.mark.parametrize("client_id", [0, 1, ])
    @pytest.mark.parametrize("name", [None, '', 'Mrs Client', ])
    def test_legacy(
        self,
        create_schema_data_dict_legacy, create_event_data_dict_legacy,
        namespace, expected_namespace,
        client_id, name
    ):
        event = _decode_avro_record_legacy(
            create_schema_data_dict_legacy(namespace),
            create_event_data_dict_legacy(client_id, name),
        )
        assert event.name == 'ClientCreated'
        assert event.namespace == expected_namespace
        assert event.source == 'urn:unspecified'
        assert event.data.get('client_id') == client_id
        assert event.data.get('name') == name


class TestAvroSerializerProtocolV1LegacyTer:

    @pytest.fixture()
    def create_schema_data_dict_legacy(self):
        def _create_schema_data_dict_legacy(field_name, field_type):
            schema_yml = f"""---
namespace: urn:service
name: SystemUpdatedEvent
type: record
fields:
# general fields
- name: correlation_id
  type:
    - "null"
    - string
- name: event_timestamp
  doc: UNIX timestamp, in milliseconds
  type:
    type: long
    logicalType: timestamp-millis
# specific fields
- name: {field_name}
  type: {field_type}
"""
            return yaml.load(schema_yml, Loader=yaml.SafeLoader)

        return _create_schema_data_dict_legacy

    @pytest.fixture()
    def create_event_data_dict_legacy(self):
        def _create_event_data_dict_legacy(field_name, field_value):
            date = datetime.fromisoformat('2021-01-01T01:01:01.001+00:00')
            return {
                'correlation_id': 'cid',
                'event_timestamp': int(date.timestamp() * 1000),
                field_name: field_value,
            }

        return _create_event_data_dict_legacy

    @pytest.mark.parametrize("field_name", ['toto', ])
    @pytest.mark.parametrize("field_type", ['str', ])
    @pytest.mark.parametrize("field_value", ['titi', ])
    def test_legacy(
        self,
        create_schema_data_dict_legacy, create_event_data_dict_legacy,
        field_name, field_type, field_value,
    ):
        event = _decode_avro_record_legacy(
            create_schema_data_dict_legacy(field_name, field_type),
            create_event_data_dict_legacy(field_name, field_value),
        )
        assert event.data[field_name] == field_value

    @pytest.mark.parametrize("field_name", ['protocol_version', ])
    @pytest.mark.parametrize("field_type", ['str', ])
    @pytest.mark.parametrize("field_value", ['titi', ])
    def test_legacy(
        self,
        create_schema_data_dict_legacy, create_event_data_dict_legacy,
        field_name, field_type, field_value,
    ):
        with pytest.raises(AvroDecodeError) as exc_info:
            _decode_avro_record_legacy(
                create_schema_data_dict_legacy(field_name, field_type),
                create_event_data_dict_legacy(field_name, field_value),
            )
        assert exc_info.value.decoded_data.get(field_name) == field_value

