# Copyright (c) Qotto, 2021
import json
import logging
import unittest
from io import StringIO

import eventy.config
from eventy.logging import GkeHandler

logger = logging.getLogger(__name__)


class TestGkeHandler(unittest.TestCase):
    """
    Test of GkeLogger implementation
    """

    def setUp(self) -> None:
        self.log_stream = StringIO()
        logging.getLogger().handlers = [GkeHandler(stream=self.log_stream)]
        logging.getLogger().setLevel(logging.INFO)

    def test_simple_log(self):
        logger.info("Hello, world!")
        log_lines = list(filter(lambda x: bool(x), self.log_stream.getvalue().split('\n')))
        self.assertEqual(len(log_lines), 1)
        payload = json.loads(log_lines[0])
        self.assertEqual(payload['severity'], 'INFO')
        self.assertEqual(payload['message'], 'Hello, world!')

    def test_erroneous_log(self):
        logger.info(
            "Hello, ",
            "world!"
        )
        log_lines = list(filter(lambda x: bool(x), self.log_stream.getvalue().split('\n')))
        self.assertEqual(len(log_lines), 2)
        err_payload = json.loads(log_lines[0])
        log_payload = json.loads(log_lines[1])

        self.assertEqual(log_payload['severity'], 'INFO')
        self.assertEqual(log_payload['logger_name'], 'tests.test_logging_handlers')
        self.assertEqual(log_payload['message'], 'Hello,  (with args: (\'world!\',))')

        self.assertEqual(err_payload['severity'], 'ERROR')
        self.assertEqual(err_payload['logger_name'], 'eventy.logging.gke_handler')
        self.assertTrue(err_payload['message'].startswith('Could not format log record from'))
