# Copyright (c) Qotto, 2021

import datetime
import unittest
from datetime import datetime, timezone

from eventy.record import Event, RecordAttributeError, Response


class TestRecord(unittest.TestCase):
    """
    Test of Record interface
    """

    def test_event_date(self):
        event = Event(
            protocol_version="3.0.0",
            namespace='urn:test:service',
            name='TestEvent',
            version='1.0.0',
            source='urn:test:service',
        )
        now = datetime.now(timezone.utc)
        self.assertTrue(isinstance(event, Event))

        event.date = now
        self.assertAlmostEqual(
            # actual
            event.date.timestamp(),
            # expected
            now.timestamp(),
            # date are stored with millisecond precision
            delta=0.001
        )

    def test_attribute_error(self):
        self.assertRaises(
            RecordAttributeError,
            lambda: Event(
                protocol_version="3.0.0",
                namespace='test:service',
                name='TestEvent',
                version='1.0.0',
                source='urn:test:service',
            )
        )
        # destination format
        self.assertRaises(
            RecordAttributeError,
            lambda: Response(
                protocol_version="3.0.0",
                namespace='urn:test:service',
                name='TestEvent',
                version='1.0.0',
                source='urn:test:service',
                request_uuid='4607d8a7-b16f-4640-9296-301a5fbb77ed',
                destination='destination',
            )
        )


    def test_attribute_missing(self):
        # Missing source
        self.assertRaises(
            TypeError,
            lambda: Event(
                protocol_version="3.0.0",
                namespace='urn:test:service',
                name='TestEvent',
                version='1.0.0',
            )
        )
        # Missing version
        self.assertRaises(
            TypeError,
            lambda: Event(
                protocol_version="3.0.0",
                namespace='urn:test:service',
                name='TestEvent',
                source='urn:test:service',
            )
        )
        # Missing name
        self.assertRaises(
            TypeError,
            lambda: Event(
                protocol_version="3.0.0",
                namespace='urn:test:service',
                version='1.0.0',
                source='urn:test:service',
            )
        )
        # Missing namespace
        self.assertRaises(
            TypeError,
            lambda: Event(
                protocol_version="3.0.0",
                name='TestEvent',
                version='1.0.0',
                source='urn:test:service',
            )
        )
        # Missing protocol_version
        self.assertRaises(
            TypeError,
            lambda: Event(
                namespace='urn:test:service',
                name='TestEvent',
                version='1.0.0',
                source='urn:test:service',
            )
        )

        # Missing destination
        self.assertRaises(
            TypeError,
            lambda: Response(
                protocol_version="3.0.0",
                namespace='urn:test:service',
                name='TestEvent',
                version='1.0.0',
                source='urn:test:service',
                request_uuid='4607d8a7-b16f-4640-9296-301a5fbb77ed',
            )
        )
        # Missing request_uuid
        self.assertRaises(
            TypeError,
            lambda: Response(
                protocol_version="3.0.0",
                namespace='urn:test:service',
                name='TestEvent',
                version='1.0.0',
                source='urn:test:service',
                destination='urn:test:other-service',
            )
        )
