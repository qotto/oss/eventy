# Copyright (c) Qotto, 2021
import logging
import unittest

logger = logging.getLogger(__name__)


def set_service_name_as_global_variable(name: str):
    from eventy.config import SERVICE_NAME
    SERVICE_NAME = name


def set_service_name_as_module_attribute(name: str):
    from eventy import config
    config.SERVICE_NAME = name


def get_service_name_as_global_variable() -> str:
    from eventy.config import SERVICE_NAME
    return SERVICE_NAME


def get_service_name_as_module_attribute() -> str:
    from eventy import config
    return config.SERVICE_NAME


class TestConfig(unittest.TestCase):
    """
    Test of eventy config
    """

    def test_set_as_module_attribute(self):
        name = 'test-name'
        set_service_name_as_module_attribute(name)
        self.assertEqual(get_service_name_as_module_attribute(), name)
        self.assertEqual(get_service_name_as_global_variable(), name)

    def test_set_as_global_variable(self):
        name = 'test-name-global'
        set_service_name_as_global_variable(name)
        self.assertEqual(get_service_name_as_module_attribute(), '-')
        self.assertEqual(get_service_name_as_global_variable(), '-')
