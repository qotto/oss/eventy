# Copyright (c) Qotto, 2021

from __future__ import annotations

import os
import unittest
from typing import Optional, Any
from uuid import uuid4

from eventy.record import Event, Response
from eventy.serialization.avro import AvroSerializer
from eventy.serialization.errors import SerializationError
from eventy.serialization.json import JsonSerializer
from eventy.trace_id.generator import gen_trace_id


def create_event():
    return Event(
        protocol_version="3.0.0",
        namespace='urn:domain:service',
        name='TestEvent',
        version='1.0.0',
        source='urn:domain:service',
        correlation_id='test_service:cid:123',
        context={
            'urn:domain:service': {
                'foo': 'bar'
            }
        },
        data={
            'a_text': 'text',
            'an_integer': 13,
        }
    )


def create_response(
    data: dict[str, Any],
    ok: bool = False,
    error_code: Optional[int] = None,
    error_message: Optional[str] = None,
):
    return Response(
        protocol_version="3.0.0",
        namespace='urn:domain:service',
        name='TestResponse',
        version='1.0.0',
        source='urn:domain:service',
        destination='urn:domain:other-service',
        request_uuid=str(uuid4()),
        correlation_id=gen_trace_id('test'),
        data=data,
        ok=ok,
        error_code=error_code,
        error_message=error_message,
    )


class TestSerialization(unittest.TestCase):
    """
    Test Record Serializers
    """

    def test_json_serialization(self):
        event = create_event()
        serializer = JsonSerializer()
        encoded = serializer.encode(event)
        decoded = serializer.decode(encoded)
        self.assertEqual(event._debug_str(), decoded._debug_str())


class TestAvroResponse(unittest.TestCase):

    def test_response_encode_raises(self):
        serializer = AvroSerializer.from_schemas_folder(os.path.abspath('tests/avro_schemas'))

        # Encode with mandatory field present
        serializer.encode(
            create_response(
                {
                    'mandatory_integer': 13,
                },
                ok=True,
            )
        )

        # Cannot encode if mandatory field is not here (but other fields presents)
        self.assertRaises(
            SerializationError, lambda:
            serializer.encode(
                create_response(
                    {
                        'optional_text': '13',
                    },
                    ok=True,
                )
            )
        )

        # But we can encode if data is completely empty
        serializer.encode(
            create_response(
                {},
                ok=False,
            )
        )
