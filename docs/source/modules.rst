API Reference
=============

.. autosummary::
    :toctree: _autosummary
    :template: module-template.rst
    :recursive:

    eventy
