Eventy messaging API
==================================

Cookbook
***********************************

Some examples of how to use the API.

.. toctree::
    :maxdepth: 2
    :glob:

    messaging_cookbook.rst

Concepts
***********************************

Records
----------------------------------

There are 3 types of records:
    * ``EVENT``
    * ``REQUEST``
    * ``RESPONSE``

The ``EVENT`` fields are:
    * ``protocol_version``: SemVer version of the Eventy protocol
    * ``name``: Name of the record
    * ``namespace``: URN of the record namespace
    * ``version``: SemVer version of the record schema
    * ``source``: URN of the record emitter
    * ``uuid``: UUID v4 of the record
    * ``correlation_id``: Correlation ID
    * ``partition_key``: Partition or database key
    * ``date``: Date of the event
    * ``context``: Propagated context data
    * ``data``: Domain data

Additionally ``REQUEST`` contains:
    * ``destination``: URN of the destination service

And ``RESPONSE`` contains:
    * ``destination``: URN of the destination service (source of corresponding request)
    * ``request_uuid``: UUID v4 of the corresponding request
    * ``ok``: Boolean status
    * ``error_code``: Integer error code if not ok
    * ``error_message``: Error message if not ok


Topics
----------------------------------

Each app has exactly 3 associated topics.
    * ``service-requests``
    * ``service-events``
    * ``service-responses``

It can read records from:
    * ``service-requests``: requests to this service
    * ``other-service-responses``: responses from other services
    * ``other-service-events`` events from other services

It can write records to:
    * ``service-responses``: responses from this service
    * ``service-events`` events from this service

.. uml:: puml/topics.puml


Handlers
----------------------------------

There are 3 delivery guarantees:
    * At Most Once
    * At Least Once
    * Exactly Once

“At Most Once” handler
+++++++++++++++++++++++++++++++++++

AtMostOnce guarantee is enough if a failure to execute is not very important.

.. code-block:: python

    @handle_event(
        service='test_service',
        name='TestEvent',
        guarantee=Guarantee.AT_MOST_ONCE,
    )
    def handle_test_event(test_event: Event) -> Iterable[Record]:
        domain_value = test_event.data['key']
        some_stat = compute(domain_value)
        yield StatEvent(some_stat)

Here, each line is executed at most once. It is possible that the ``StatEvent`` will not be produced, it is possible that it is produced once, but it can not be produced twice.

“At Least Once” handler
++++++++++++++++++++++++++++++

**Idempotent code is safer here, since the code block can be executed multiple times.**

.. code-block:: python

    @handle_event(
        service='test_service',
        name='TestEvent',
        guarantee=Guarantee.AT_LEAST_ONCE,
    )
    def handle_test_event(test_event: Event) -> Iterable[Record]:
        domain_value = test_event.data['key']
        local_repository.set_some_value(domain_value)
        yield SomeEvent()

Here, each line of the handler can be executed multiple times, the guarantee is that at least one execution went through the end of the handler.

``SomeEvent`` will be produced at least once, but possibly more.

“Exactly Once” handler
+++++++++++++++++++++++++++++

**Idempotent code is safer here, since the code block can be executed multiple times.**

.. code-block:: python

    @handle_event(
        service='test_service',
        name='TestEvent',
        guarantee=Guarantee.EXACTLY_ONCE,
    )
    def handle_test_event(test_event: Event) -> Iterable[Record]:
        domain_value = test_event.data['key']
        local_repository.set_some_value(domain_value)
        info = local_repository.get_some_info()
        yield SomeEvent(info)
        this_function_can_be_executed_multiple_times()


This is a bit tricky:
    * Each line is executed at least once
    * SomeEvent is produced exactly once

So we still need the handler to be idempotent for every line except the production of the result event.


API Overview
----------------------------------

.. uml:: puml/api_overview.puml

