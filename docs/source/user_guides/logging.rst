Logging and tracing
===================

Trace variables
---------------

Eventy defines a concept of "trace variables" to hold trace ids. These trace ids are context-bound and can be used to automatically add traces in your logs or API calls.

* ``correlation_id``: correlate multiple services involved in the same business operation
* ``request_id``: identify a single request on a service

Direct modification of trace variables::

    from eventy.trace_id import correlation_id_var, request_id_var

    correlation_id_var.set('my_correlation_id')
    request_id_var.set('my_request_id')

Using eventy to generate trace variables::

    from eventy.trace_id import gen_trace_id, local_trace

    with local_trace(correlation_id=gen_trace_id('cid')):
        # do something

API reference:

.. autosummary:: eventy.trace_id


Logging handlers
----------------

Eventy provides logging handlers to integrate trace variables automatically in your logs.

* ``SimpleHandler``: with a format similar to :class:`logging.StreamHandler`
* ``GkeHandler``: with a Google Kubernetes Engine (GKE) friendly format

API reference:

.. autosummary:: eventy.logging


Usage example
-------------

The following example defines a simple request handler. We use eventy to make the request id available in logs, without having to pass any information explicitly to the request handler.

.. code-block:: python
    :emphasize-lines: 26,29
    :linenos:

    import asyncio
    import logging

    from eventy.trace_id import request_id_var
    from eventy.logging import SimpleHandler

    logger = logging.getLogger(__name__)

    root_logger = logging.getLogger()
    root_logger.setLevel('DEBUG')
    root_logger.addHandler(SimpleHandler())

    async def handle_request(delay):
        logger.info(f'Begin')
        await asyncio.sleep(delay)
        logger.info(f'End')


    async def main():
        tasks = []

        request_id_var.set("R-1")
        tasks.append(asyncio.create_task(handle_request(1.0)))

        request_id_var.set("R-2")
        tasks.append(asyncio.create_task(handle_request(2.0)))

        request_id_var.set("R-3")
        tasks.append(asyncio.create_task(handle_request(1.5)))

        for task in tasks:
            await task


    asyncio.run(main())

In particular you can notice that request are executed asynchronously, and their executions overlap, but the context bound traces are still accurate.

.. code-block::
    :emphasize-lines: 5,6
    :linenos:

    [2021-07-05 14:33:48 +0200] [CID:] [RID:R-1] INFO __main__ (ex7 L19) Begin
    [2021-07-05 14:33:48 +0200] [CID:] [RID:R-2] INFO __main__ (ex7 L19) Begin
    [2021-07-05 14:33:48 +0200] [CID:] [RID:R-3] INFO __main__ (ex7 L19) Begin
    [2021-07-05 14:33:49 +0200] [CID:] [RID:R-1] INFO __main__ (ex7 L21) End
    [2021-07-05 14:33:50 +0200] [CID:] [RID:R-3] INFO __main__ (ex7 L21) End
    [2021-07-05 14:33:50 +0200] [CID:] [RID:R-2] INFO __main__ (ex7 L21) End

