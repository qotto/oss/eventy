Integration in applications
===========================

These integrations allow to seamlessly integrate eventy tracing and logging capabilities with third parties frameworks and libraries.

The correlation id should be propagated using:
 * ``x-correlation-id`` header for all HTTP communications to propagate the correlation id
 * ``correlation_id`` task parameter for all Celery communications
 * ``correlation_id`` event field for all Kafka communications

A request id should be created for each entry point of a service:
 * A new celery task
 * A new HTTP request
 * A new event

API reference:

.. autosummary:: eventy.integration


Celery integration
------------------

``tasks.py``::

    from eventy.integration.celery import send_task, shared_task
    import eventy.config

    event.config.SERVICE_NAME = 'my_service'

    @shared_task
    def my_task(param):
        logger.info(f'param={param}')
        send_task(
            'other.service.task.name', [], {
                processed_param: process(param)
            }
        )

Here the log output would be similar to (assuming you are also using eventy logging capabilities)::

    [2021-07-06 12:16:35 +0000] [CID:my_service:my_task:K2XE7IGnoOI] [RID:my_service:my_task:KHcGIz59yyb] INFO my_service.tasks (tasks L123) param='value'

API reference:

.. autosummary:: eventy.integration.celery

HTTP client: requests
---------------------

API reference:

.. autosummary:: eventy.integration.requests

HTTP server: django
---------------------

API reference:

.. autosummary:: eventy.integration.django

HTTP server: sanic
---------------------

API reference:

.. autosummary:: eventy.integration.sanic


HTTP client and server: aiohttp
------------------------------------

API reference:

.. autosummary:: eventy.integration.aiohttp
