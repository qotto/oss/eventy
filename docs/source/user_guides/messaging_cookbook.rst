Messaging Cookbook
=====================

EventyApp Quickstart
----------------------------------

If you need to customize Eventy

.. code-block:: python

    # somewhere.py

    class QottoService(Service):
        def __init__(self, name:str):
            super().__init__(
                name=name,
                namespace='urn:qotto:{name}'
            )



Initialize an App, with a RecordStore and some Agents

.. code-block:: python

    # init.py

    my_backend = MyBackend(settings.CONFIG_BACKEND, ...)

    app = EventyApp(
        record_store=ConfluentKafkaStore(settings.CONFIG_KAFKA, ...),
        app_service=QottoService('my_service'),
        ext_services=[
            QottoService('some_service'),
            QottoService('some_other_service'),
        ],
        agents=[
            MyAgent(some_backend=some_backend),
            SomeOtherAgent(),
        ],
        handlers=[
            SomeHandlers(), # If there are handlers outside agents.
        ],
    )

    app.run()



Agents with Handlers

.. code-block:: python

    # my_agent.py

    class MyAgent(Agent):
        def __init__(self, some_backend):
            self.some_backend = some_backend

        @handle_event(
            'some_service', 'SomeEvent', Guarantee.AMO
        )
        def handle_some_service_event(
            self, event: Event
        ) -> Iterable[Record]:
            some_data = event.data['some_key']
            yield ProcessedEvent(some_data)
            if some_condition():
                # we can yield multiple records
                yield ProcessedSpecialEvent(some_data)

        @handle_request(
            'MyServiceRequest', Guarantee.EOS
        )
        def handle_my_service_request(
            self, request: Request
        ) -> Iterable[Record]:
            data = request.data['key']
            result = self.some_backend.process(some_data)
            # If we can answer immediately
            if some_condition(result):
                yield MyServiceResponse(result)
            # If we need to ask something before
            # -> we will use the response (below)
            else:
                yield SomeOtherServiceRequest(result)

        @handle_response(
            'some_other_service', 'SomeOtherServiceResponse', Guarantee.EOS
        )
        def handle_some_other_service_response(
            self, response: Response
        ) -> Iterable[Record]:
            if response.ok:
                ...
            else:
                ...
            result = ...
            # define response to MyServiceRequest
            yield MyServiceResponse(result)


Produce a Record - Not within a handler
---------------------------------------

.. uml:: puml/produce_outside_handler.puml

NOTE: In this example we put ``phone_number`` and ``payment_amount`` in context because this data is not in the ``SmsPaymentResponse`` (reason is not relevant here, we can just assume there is some context we want to access)


### Using a RecordWriteStore

An Eventy App can provide a ``RecordWriteStore`` which knows how to route Records.

In the following example we let the agent create the record (to use e.g. the context in one place) and we give the http server the write store.


.. code-block:: python

    # routes.py

    class SmsRoutes:

        def __init__(
            self,
                write_store: RecordWriteStore,
                sms_payment_agent: SmsPaymentAgent,
            ):
            self.write_store = write_store
            self.sms_payment_agent

        async def sms(self, request: web.Request) -> web.Response:
            # extract JSON fields
            phone_number = ...
            payment_amount = ...
            transaction_id = ...

            # produce request
            self.write_store.write_record(
                self.sms_payment_agent.make_request(
                    phone_number, pyament_amount, transaction_id,
                )
            )

            # http response
            return web.Response(text='OK')

.. code-block:: python

    # agents.py

    class SmsPaymentAgent(Agent):

        def __init__(self):
            self.context_key = 'urn:qotto-test:gw-sms'


        def make_request(
            phone_number, payment_amount, transaction_id
        ) -> Request:
            return SmsPaymentRequest(
                    phone_number, payment_amount, transaction_id,
                    context={
                        self.context_key: {
                            'phone_number': phone_number,
                            'payment_amount': payment_amount
                        }
                    }
            )

        @handle_response(
            'payment', 'SmsPaymentResponse', Guarantee.EXACTLY_ONCE
        )
        def handle_response(self, response: Response) -> Iterable[Record]:
            ...


.. code-block:: python

    # init.py

    # Record store
    record_store = CKStore(...)

    # Agents
    sms_payment_agent = SmsPaymentAgent(...)

    # configure eventy app
    eventy_app = EventyApp(
        record_store=record_store,
        app_service=...,
        ext_services=[...],
        agents=[sms_payment_agent, ...]
    )

    # configure http server
    routes = SmsRoutes(sms_payment_agent, eventy_app.get_write_store())
    http_app = application()
    http_app.add_routes(
        [
            web.post('/sms', routes.sms),
        ]
    )


Eventy App and Celery
---------------------

This example shows how to use restart a service using eventy and celery in case eventy fails.

.. code-block:: python

    celery_app = Celery(...)
    eventy_app = EventyApp(...)

    def terminate_celery(exc=None):
        if exc:
            logger.error(f"Eventy App failed, Celery warm shutdown...")
            signal.raise_signal(signal.SIGTERM)
            sleep(30)
            logger.warning("Celery app still running, will force shutdown.")
            sys.exit(1)
        else:
            logger.info("Eventy App was cancelled.")

    eventy_app.run(
        done_callback=terminate_celery,
    )


Eventy App and Sanic
--------------------

This example shows how to use restart a service using eventy and sanic in case eventy fails.

.. code-block:: python

    sanic_server = await sanic_app.create_server(..., return_asyncio_server=True)

    def on_eventy_done(exc=None):
        if exc:
            logger.error(f"Eventy App failed, Sanic warm shutdown...")
            sanic_server.close()
            sanic_server.wait_closed()
        else:
            logger.info(f"Eventy App was cancelled.")

    eventy_app.run(done_callback=on_eventy_done)

    await sanic_server.startup()
    await sanic_server.start_serving()
    await sanic_server.wait_closed()
