Eventy documentation
========================================


What is eventy?
---------------

Eventy is both a protocol and a library for making the design of fault-tolerant, event-driven, concurrent and distributed applications in a microservices-oriented architecture easier.

Eventy was created and is maintained by `Qotto <http://www.qotto.net>`_

 * Source: `Eventy on GitLab <https://gitlab.com/qotto/oss/eventy>`_
 * Package: `Eventy on Pypi <https://pypi.org/project/eventy/>`_
 * Documentation: `Eventy API documentation <https://qotto.gitlab.io/oss/eventy/>`_


Quickstart
----------

Install with poetry::

    poetry add eventy

Install with optional dependencies::

    poetry add eventy[celery, kafka, sanic, aiohttp, requests, django, confluent-kafka, avro]

You can now browse our user guides to get started

.. toctree::
    :maxdepth: 2
    :caption: User guides:
    :glob:

    user_guides/logging.rst
    user_guides/integration.rst
    user_guides/messaging.rst

.. toctree::
    :maxdepth: 5
    :caption: Complete API:

    modules.rst

Modules:
---------

* :ref:`modindex`

Index:
---------

* :ref:`genindex`
