include docs/Makefile

.PHONY: help clean build publish_test publish tests integration-tests coverage docker-docs docker-tests tox

help:
	@echo "make help            - Display this help message"
	@echo "make clean           - Clean up build files"
	@echo "make build           - Build pypi package"
	@echo "make docs            - Build documentation in docs/ with sphinx"
	@echo "make tests           - Run tests and detects code issues"
	@echo "make coverage        - Create a test coverage report"
	@echo "make publish_test    - Publish Pypi package (in TestPyPi)"
	@echo "make publish         - Publish Pypi package"
	@echo "make docker-docs     - Build documentation in docker"
	@echo "make docker-tests    - Run tests in docker"


clean:
	rm -fr dist *.egg-info .mypy_cache .tox docs/build docs/source/_autosummary/*
	find . -type d -name '__pycache__' -exec rm -fr {} +

build:
	poetry build

type-check:
	mypy

linter:
	flake8 eventy

unit-tests:
	pytest --doctest-modules eventy
	pytest

tox:
	tox

integration-tests:
	bash integration_tests/run_integration_tests.sh

tests: type-check linter unit-tests

publish_test:
	poetry publish -r testpypi

publish:
	poetry publish


coverage:
	mypy eventy
	flake8 eventy
	coverage run --source='eventy' tests
	coverage report -m --fail-under=50 --skip-empty
	coverage xml

docker-tests:
	docker build . -f Dockerfile.tests.py38
	docker build . -f Dockerfile.tests.py39
	docker build . -f Dockerfile.tests.py310

docker-docs:
	$(eval GIT_COMMIT = $(shell git rev-parse HEAD))
	docker build . -f Dockerfile.docs -t $(GIT_COMMIT)-docs
	docker run $(GIT_COMMIT)-docs cat docs.tar > docs.tar
	mkdir result/
	tar xf docs.tar -C result/
