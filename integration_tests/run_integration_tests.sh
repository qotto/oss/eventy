#!/usr/bin/env bash

cd "$(dirname ${BASH_SOURCE[0]})" || exit 1

function run_test() {
  TEST_NAME="$1"
  OUT="tmp_$TEST_NAME.out"
  ERR="tmp_$TEST_NAME.err"
  echo "Integration Test:  $TEST_NAME ..."

  bash ./$TEST_NAME/run_integration_test.sh 1>$OUT 2>$ERR
  TEST_STATUS=$?

  if [ $TEST_STATUS -eq 0 ] ; then
    echo "> Success"
  else
    echo "> FAILED"
    echo "  standard output was: $OUT"
    echo "  error    output was: $ERR"
  fi
}



run_test sanic
run_test django
run_test celery
run_test requests
run_test aiohttp
run_test celery_eventy_app
run_test sanic_eventy_app
run_test django_eventy_app
run_test django_eventy_app_health
