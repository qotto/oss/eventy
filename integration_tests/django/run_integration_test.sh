#!/usr/bin/env bash

cd "$(dirname ${BASH_SOURCE[0]})" || exit 1

# Django Unit Test Framework
# --------------------------

./manage.py test
UNITTEST_STATUS=$?

if [ $UNITTEST_STATUS -eq 0 ]; then
  echo "SUCCESS: ./manage.py test succeeded."
  echo "--"
else
  exit 1
fi

# Real app testing
# ----------------
#
# Could not provide arbitrary HEADERS in Django Unit Test Framework.
# This test checks that x-correlation-id is read correctly


echo "start django app..."
python manage.py runserver &> server_output &
pid=$!

sleep 1
echo "django app warmup..."
sleep 1

echo "curl 127.0.0.1:8000/hello -H 'x-correlation-id: my_corr_id'"
RESULT=$(curl 127.0.0.1:8000/hello --data '6bytes' -H 'x-correlation-id: my_corr_id' 2>/dev/null)
echo "Result was: $RESULT, will grep my_corr_id"
echo "$RESULT" | grep "my_corr_id"
CORR_ID_STATUS=$?
cat server_output | grep "Request: POST /hello (6)"
REQUEST_LENGTH_STATUS=$?

echo "rm server_output..."
rm server_output

echo "kill django app..."
sleep 1
kill $pid
KILL_STATUS=$?
if [ $KILL_STATUS -eq 0 ]; then
  echo "Killed django app"
else
  echo "ERROR: Django App was not running."
  exit 1
fi

if [ $CORR_ID_STATUS -eq 0 ]; then
  echo "OK: Correlation ID in header"
else
  echo "FAILED: Correlation ID in header"
  exit 1
fi

if [ $REQUEST_LENGTH_STATUS -eq 0 ]; then
  echo "OK: Request Length"
else
  echo "FAILED: Request Length"
  exit 1
fi

exit 0
