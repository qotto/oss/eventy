from __future__ import annotations

import logging
from enum import Enum

from environs import Env

import eventy.integration.aiohttp
import eventy.integration.requests
from eventy.integration.celery import send_task

logger = logging.getLogger(__name__)

env = Env()

STORE = env.enum(
    'STORE',
    type=Enum('STORE', 'memory kafka'),
    ignore_case=True,
    default='kafka',
).name

APP = env.enum(
    'APP',
    type=Enum('APP', 'celery aiohttp django'),
    ignore_case=True,
    default='celery',
).name

logger.info(f"Using app {APP} with store {STORE}.")


class Client:
    def send(self, store: str):
        raise NotImplementedError()


class CeleryClient(Client):
    def send(self, store: str):
        task: str
        if store == 'memory':
            task = 'celery_app.celery_tasks.send_event_memory'
        elif store == 'kafka':
            task = 'celery_app.celery_tasks.send_event_kafka'
        else:
            raise ValueError(f"Unknown store {store}.")
        result = send_task(
            task, kwargs={'name': 'Toto', }
        ).get()
        logger.info(f"Task {task} has result {result}.")


class AiohttpClient(Client):
    def send(self, store: str):
        url: str
        if store == 'memory':
            url = 'http://localhost:8080/send_event_memory'
        elif store == 'kafka':
            url = 'http://localhost:8080/send_event_kafka'
        else:
            raise ValueError(f"Unknown store {store}.")
        result = eventy.integration.requests.post(
            url=url, json={'name': 'Toto'}
        )
        logger.info(f"Aiohttp POST {url} has result {result.text}.")


class DjangoClient(Client):
    def send(self, store: str):
        url: str
        if store == 'memory':
            url = 'http://localhost:8000/send_event_memory'
        elif store == 'kafka':
            url = 'http://localhost:8000/send_event_kafka'
        else:
            raise ValueError(f"Unknown store {store}.")
        result = eventy.integration.requests.post(
            url=url, json={'name': 'Toto'}
        )
        logger.info(f"Django POST {url} has result {result.text}.")


client: Client
if APP == 'celery':
    client = CeleryClient()
elif APP == 'aiohttp':
    client = AiohttpClient()
elif APP == 'django':
    client = DjangoClient()
else:
    raise ValueError(f"Unknown app {APP}.")

client.send(STORE)
