import logging

import eventy.config
from client import celery_config
from eventy.integration.celery import Celery
from eventy.logging import SimpleHandler

eventy.config.SERVICE_NAME = 'celery-client'

logging.getLogger().setLevel('DEBUG')
logging.getLogger().addHandler(SimpleHandler(colored=True))

celery_app = Celery('celery-client')
celery_app.config_from_object(celery_config)
