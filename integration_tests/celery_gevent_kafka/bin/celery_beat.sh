#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})/.." || exit 1

echo "Starting celery beat..."

IS_CELER_BEAT=1 exec /usr/bin/env celery -A celery_app beat -l debug
