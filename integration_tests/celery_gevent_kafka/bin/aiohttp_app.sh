#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})/.." || exit 1

echo "Starting AIOHTTP app..."

python -m aiohttp_app
