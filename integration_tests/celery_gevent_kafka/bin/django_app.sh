#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})/../django_app" || exit 1

echo "Starting Django app..."

python manage.py runserver
