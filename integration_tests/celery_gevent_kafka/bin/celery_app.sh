#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})/.." || exit 1

echo "Starting celery app..."

exec /usr/bin/env celery -A celery_app.tasks worker --without-gossip --without-mingle --pool gevent --concurrency 4 -n app@%h -Q celery -l debug
