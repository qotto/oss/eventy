import logging

from eventy.integration.celery import shared_task, Celery
from eventy.record import Event
from . import celery_config
from .eventy_app import kafka_write_store, memory_write_store

logger = logging.getLogger(__name__)

celery_app = Celery('celery-app')
celery_app.config_from_object(celery_config)
celery_app.autodiscover_tasks()

@shared_task
def hello(name: str) -> str:
    logger.info(f"Hello task with name {name}")
    return f'Hello {name}'


@shared_task
def send_event_memory(name: str) -> str:
    memory_write_store.write(
        Event(
            name='HelloEvent',
            namespace='urn:test:celery-app',
            protocol_version='3.0.0',
            version='1.0.0',
            source='urn:test:celery-app',
            data={'name': name}
        ),
    )
    return f'Hello {name}'


@shared_task
def send_event_kafka(name: str) -> str:
    kafka_write_store.write(
        Event(
            name='HelloEvent',
            namespace='urn:test:celery-app',
            protocol_version='3.0.0',
            version='1.0.0',
            source='urn:test:celery-app',
            data={'name': name}
        ),
    )
    return f'Hello {name}'


@shared_task
def beat_task():
    logger.info("Beat task.")
    name = 'beat'
    kafka_write_store.write(
        Event(
            name='HelloEvent',
            namespace='urn:test:celery-app',
            protocol_version='3.0.0',
            version='1.0.0',
            source='urn:test:celery-app',
            data={'name': name}
        ),
    )
    return f'Hello {name}'
