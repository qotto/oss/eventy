import logging
from threading import Thread
from time import sleep

import eventy.config
from celery_app import celery_config
from eventy.integration.celery import Celery
from eventy.logging import SimpleHandler

eventy.config.SERVICE_NAME = 'celery-app'

logging.getLogger().setLevel('DEBUG')
logging.getLogger().addHandler(SimpleHandler(colored=True))


