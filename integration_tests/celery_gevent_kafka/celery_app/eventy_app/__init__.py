import logging
import os

from eventy.messaging import EventyApp, Service, RecordWriteStore
from eventy.messaging.confluent_kafka import CKStore
from eventy.messaging.memory import MemoryStore
from eventy.record import Record
from eventy.serialization.json import JsonSerializer

kafka_eventy_app = EventyApp(
    CKStore(
        serializer=JsonSerializer(),
        bootstrap_servers=["localhost:9092"],
        group_id="eventy_test",
    ),
    Service('celery-app-kafka', event_topic='test-events', request_topic='test-requests'),
    read_timeout_ms=5000,
    read_wait_time_ms=5000,
)

memory_eventy_app = EventyApp(
    MemoryStore(),
    Service('celery-app-memory', event_topic='test-events', request_topic='test-requests'),
    read_timeout_ms=5000,
    read_wait_time_ms=5000,
)
if 'IS_CELERY_BEAT' not in os.environ:
    kafka_eventy_app.run()
    memory_eventy_app.run()

    kafka_write_store = kafka_eventy_app.write_store
    memory_write_store = memory_eventy_app.write_store
else:
    logging.getLogger(__name__).info('Running celery beat')


    class NullRecordWriteStore(RecordWriteStore):
        def write(self, record: Record) -> None:
            logging.getLogger(__name__).error(
                f"************************************************************************\n"
                f"Writing record {record}"
            )

    kafka_write_store = NullRecordWriteStore()
    memory_write_store = NullRecordWriteStore()

