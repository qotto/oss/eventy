import logging

import eventy.config
import eventy.config.aiohttp
from eventy.integration.aiohttp import application
from eventy.logging import SimpleHandler

eventy.config.SERVICE_NAME = 'aiohttp_web'
eventy.config.aiohttp.AIOHTTP_WEB_ACCESS_DISABLE_HEALTH_LOGGING = True

# setup logging
logging.getLogger().setLevel('DEBUG')
logging.getLogger().addHandler(SimpleHandler(colored=True))

# setup aiohttp app
http_app = application()
