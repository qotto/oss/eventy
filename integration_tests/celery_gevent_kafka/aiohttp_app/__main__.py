"""
Minimal HTTP server to check that x-correlation-id is present
"""
import logging

import aiohttp.web

from eventy.record import Event
from eventy.trace_id import correlation_id_var
from http_app import http_app
from http_app.eventy_app import memory_write_store, kafka_write_store

logger = logging.getLogger(__name__)


async def health(request):
    return aiohttp.web.json_response(text="OK")


async def hello(request):
    return aiohttp.web.json_response(text=correlation_id_var.get())


async def send_event_memory(request: aiohttp.web.Request):
    try:
        json = await request.json()
        name = json['name']
    except Exception as e:
        logger.error(f"Bad request: {e}")
        return aiohttp.web.json_response(
            text="Bad request",
            status=400
        )
    logger.info(f"Sending event to memory with name = {name}")
    memory_write_store.write(
        Event(
            name='HelloEvent',
            namespace='urn:test:celery-app',
            protocol_version='3.0.0',
            version='1.0.0',
            source='urn:test:http-app',
            data={'name': name}
        ),
    )
    return aiohttp.web.json_response(text=f'OK memory name {name}')


async def send_event_kafka(request: aiohttp.web.Request):
    try:
        json = await request.json()
        name = json['name']
    except Exception as e:
        logger.error(f"Bad request: {e}")
        return aiohttp.web.json_response(
            text="Bad request",
            status=400
        )
    logger.info(f"Sending event to kafka with name = {name}")
    kafka_write_store.write(
        Event(
            name='HelloEvent',
            namespace='urn:test:celery-app',
            protocol_version='3.0.0',
            version='1.0.0',
            source='urn:test:http-app',
            data={'name': name}
        ),
    )
    return aiohttp.web.json_response(text=f'OK kafka name {name}')


http_app.add_routes(
    [
        aiohttp.web.get('/health', health),
        aiohttp.web.get('/hello', hello),
        aiohttp.web.post('/send_event_memory', send_event_memory),
        aiohttp.web.post('/send_event_kafka', send_event_kafka),
    ]
)

aiohttp.web.run_app(http_app)
