import json
import logging

from django.http import HttpResponse, JsonResponse, HttpRequest
from django.views.decorators.http import require_GET

from app.eventy_app import memory_write_store, kafka_write_store
from eventy.record import Event
from eventy.trace_id import correlation_id_var

logger = logging.getLogger(__name__)


@require_GET
def health(request):
    return HttpResponse('OK')


def hello(request):
    return JsonResponse({'hello': correlation_id_var.get()})


def send_event_memory(request: HttpRequest):
    try:
        json_data = json.loads(request.body)
        name = json_data['name']
    except Exception as e:
        logger.error(f"Bad request: {e}")
        return HttpResponse(
            text="Bad request",
            status=400
        )
    logger.info(f"Sending event to memory with name = {name}")
    memory_write_store.write(
        Event(
            name='HelloEvent',
            namespace='urn:test:celery-app',
            protocol_version='3.0.0',
            version='1.0.0',
            source='urn:test:http-app',
            data={'name': name}
        ),
    )
    return JsonResponse(data={'ok': f'OK memory name {name}'})


def send_event_kafka(request: HttpRequest):
    try:
        json_data = json.loads(request.body)
        name = json_data['name']
    except Exception as e:
        logger.error(f"Bad request: {e}")
        return HttpResponse(
            text="Bad request",
            status=400
        )
    logger.info(f"Sending event to kafka with name = {name}")
    kafka_write_store.write(
        Event(
            name='HelloEvent',
            namespace='urn:test:celery-app',
            protocol_version='3.0.0',
            version='1.0.0',
            source='urn:test:http-app',
            data={'name': name}
        ),
    )
    return JsonResponse(data={'ok': f'OK kafka name {name}'})
