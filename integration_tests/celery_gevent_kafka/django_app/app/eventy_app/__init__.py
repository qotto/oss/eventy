from eventy.messaging import EventyApp, Service
from eventy.messaging.confluent_kafka import CKStore
from eventy.messaging.memory import MemoryStore
from eventy.serialization.json import JsonSerializer

kafka_eventy_app = EventyApp(
    CKStore(
        serializer=JsonSerializer(),
        bootstrap_servers=["localhost:9092"],
        group_id="eventy_test",
    ),
    Service('celery-app-kafka', event_topic='test-events', request_topic='test-requests'),
    read_timeout_ms=5000,
)

memory_eventy_app = EventyApp(
    MemoryStore(),
    Service('celery-app-memory', event_topic='test-events', request_topic='test-requests'),
    read_timeout_ms=5000,
)

kafka_eventy_app.run()
memory_eventy_app.run()

kafka_write_store = kafka_eventy_app.write_store
memory_write_store = memory_eventy_app.write_store
