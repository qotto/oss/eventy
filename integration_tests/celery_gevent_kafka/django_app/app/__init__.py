import logging

from app import settings

import eventy.config
import eventy.config.django
from eventy.logging import GkeHandler, SimpleHandler

# Configure Eventy
eventy.config.SERVICE_NAME = 'eventy-django-test'
# Django integration
eventy.config.django.DJANGO_ACCESS_HEALTH_ROUTE = '/health'
eventy.config.django.DJANGO_ACCESS_DISABLE_HEALTH_LOGGING = settings.SKIP_HEALTHCHECK_LOGGING

# Setup logging
logging.getLogger().addHandler(SimpleHandler(colored=settings.COLORED_LOGGING))
logging.getLogger().setLevel('DEBUG')
