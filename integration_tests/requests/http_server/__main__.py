"""
Minimal HTTP server to check that x-correlation-id is present
"""
import logging

from sanic.response import text

import eventy.config
from eventy.integration.sanic import Sanic
from eventy.logging import SimpleHandler
from eventy.trace_id import correlation_id_var, user_id_var

# Configure Eventy
eventy.config.SERVICE_NAME = 'http_server'

# Setup logging
root_logger = logging.getLogger()
root_logger.setLevel('INFO')
root_logger.addHandler(SimpleHandler(colored=True))

# Setup Sanic app
app = Sanic("TestSanicServer")


@app.route('/hello')
async def hello(request):
    return text(correlation_id_var.get() + "|" + user_id_var.get())


if __name__ == '__main__':
    app.run()
