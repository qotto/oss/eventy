import eventy.integration.requests
from eventy.integration.requests import get
from eventy.trace_id import correlation_id_var, user_id_var

print("Test: No header")
response = get('http://0.0.0.0:8000/hello').text
if response.startswith('http_server:/hello'):
    print("OK")
else:
    print("FAILED")
    exit(1)

print("Test: Header with global get method")
correlation_id_var.set('my_correlation_id_global')
user_id_var.set('my_user_id_global')
response = get('http://0.0.0.0:8000/hello').text
if response == 'my_correlation_id_global|my_user_id_global':
    print("OK")
else:
    print("FAILED")
    exit(1)

print("Test: Header with session get method")
correlation_id_var.set('my_correlation_id_session')
user_id_var.set('my_user_id_session')
with eventy.integration.requests.Session() as session:
    response = get('http://0.0.0.0:8000/hello').text
    if response == 'my_correlation_id_session|my_user_id_session':
        print("OK")
    else:
        print("FAILED")
        exit(1)
