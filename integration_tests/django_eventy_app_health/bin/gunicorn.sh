#!/bin/bash

BASE_DIR=$(dirname "$BASH_SOURCE")
BASE_DIR=$(dirname "$BASE_DIR")
export PYTHONPATH=$PYTHONPATH:$BASE_DIR
PID_FILE=$BASE_DIR/gunicorn.pid
exec /usr/bin/env gunicorn app.wsgi -b 0.0.0.0:8000 --preload --worker-class gthread --workers 2 --threads 5 --max-requests 1000 --max-requests-jitter 200 --pid $PID_FILE
