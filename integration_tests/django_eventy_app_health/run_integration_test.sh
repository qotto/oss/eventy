#!/usr/bin/env bash

cd "$(dirname ${BASH_SOURCE[0]})" || exit 1


# Real app testing
# ----------------
#
# Could not provide arbitrary HEADERS in Django Unit Test Framework.
# This test checks that x-correlation-id is read correctly


echo "start django app..."
./bin/gunicorn.sh &> server_output &
APP_PID=$!

sleep 1
echo "django app warmup..."
sleep 1

function curl_cmd() {
  CURL_CMD="$1"
  >&2 echo -n "run curl command: $CURL_CMD"
  RESULT=$($CURL_CMD 2>/dev/null)
  >&2 echo " >CURL RESULT> $RESULT"
  echo "$RESULT"
}

OUTPUT=$(curl_cmd "curl 127.0.0.1:8000/send_ok_event")
STATUS_OK=$(curl_cmd "curl 127.0.0.1:8000/health")
OUTPUT=$(curl_cmd "curl 127.0.0.1:8000/send_error_event")
STATUS_KO=$(curl_cmd "curl 127.0.0.1:8000/health")

if kill -0 $APP_PID
then
  echo "Django app still alive, it's OK."
  kill $APP_PID
else
  ps
  echo "APP_PID=$APP_PID"
fi

echo "rm server_output..."
rm server_output

if [ "$STATUS_OK" == "OK" ] && [ "$STATUS_KO" == "eventy_app is not healthy" ]
then
  echo "Integration Test:  django_eventy_app ... Success"
  exit 0
else
  echo "Integration Test:  django_eventy_app ... FAILED"
  exit 1
fi
