"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

import logging

from app import views
from app.eventy import eventy_app, record_store
from django.conf.urls import url

logger = logging.getLogger(__name__)

statuses = dict(eventy_app=True)

urlpatterns = [
    url(r'^send_ok_event', views.get_send_ok_event_view(record_store)),
    url(r'^send_error_event', views.get_send_error_event_view(record_store)),
    url(r'^health', views.get_health_view(statuses)),
]

eventy_app.run(
    done_callback=lambda *args, **kwargs: statuses.update(eventy_app=False),
)
