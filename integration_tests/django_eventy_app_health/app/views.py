from django.http import HttpResponse

from eventy.messaging import RecordStore
from eventy.messaging.memory import MemoryStore
from eventy.record import Event
from app.eventy import EventyAgent


def get_send_ok_event_view(record_store: MemoryStore):
    def send_ok_event(request):
        record_store.add_record(
            Event(
                protocol_version='3.0.0',
                namespace='urn:test-django-eventy-app-ext',
                name='TestEvent',
                version='1.0.0',
                source='urn:test-django-eventy-app-ext',
                data={'ok': True}
            ),
            'test-topic'
        )
        return HttpResponse(f"Sent ok event")

    return send_ok_event


def get_send_error_event_view(record_store: MemoryStore):
    def send_error_event(request):
        record_store.add_record(
            Event(
                protocol_version='3.0.0',
                namespace='urn:test-django-eventy-app-ext',
                name='TestEvent',
                version='1.0.0',
                source='urn:test-django-eventy-app-ext',
                data={'ok': False}
            ),
            'test-topic'
        )
        return HttpResponse("Sent error event")

    return send_error_event


def get_health_view(statuses: dict[str, bool]):
    def health(request):
        for name, status in statuses.items():
            if not status:
                return HttpResponse(f"{name} is not healthy", status=500)
        return HttpResponse("OK")

    return health
