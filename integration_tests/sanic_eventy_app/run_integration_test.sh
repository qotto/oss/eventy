#!/usr/bin/env bash

cd "$(dirname ${BASH_SOURCE[0]})" || exit 1

echo "start sanic app..."
python app &
APP_PID=$!

sleep 1
echo "sanic app warmup..."
sleep 1

function curl_cmd() {
  CURL_CMD="$1"
  >&2 echo -n "run curl command: $CURL_CMD"
  RESULT=$($CURL_CMD 2>/dev/null)
  >&2 echo " >CURL RESULT> $RESULT"
}

OUTPUT=$(curl_cmd "curl 127.0.0.1:8000/hello")
OUTPUT=$(curl_cmd "curl 127.0.0.1:8000/send_ok_event")
OUTPUT=$(curl_cmd "curl 127.0.0.1:8000/eventy_alive")

# This makes eventy app crash
OUTPUT=$(curl_cmd "curl 127.0.0.1:8000/send_error_event")

sleep 5 # Give some time for sanic to shutdown

if kill -0 $APP_PID
then
  echo "Sanic app still alive, it should not be."
  kill $APP_PID
  echo FAILED
  exit 1
else
  ps
  echo "APP_PID=$APP_PID"
  echo OK
  exit 0
fi
