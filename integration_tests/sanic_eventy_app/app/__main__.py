import asyncio
import logging

from sanic.response import text

import eventy.config
from eventy.integration.sanic import Sanic
from eventy.logging import SimpleHandler
from eventy.messaging import EventyApp, Service, Agent, handle_event
from eventy.messaging.memory import MemoryStore
from eventy.record import Event

eventy.config.SERVICE_NAME = 'test-sanic-eventy-app'

logging.getLogger().setLevel('INFO')
logging.getLogger().addHandler(SimpleHandler(colored=True))

logger = logging.getLogger(__name__)


class EventyAgent(Agent):
    @handle_event(service='test-sanic-eventy-app-ext', name='TestEvent')
    def handle(self, event: Event):
        if event.data.get('ok'):
            logger.info("EventyAgent: TestEvent: OK")
        else:
            logger.error("EventyAgent: TestEvent: ERROR")
            raise Exception('EventyAgent: TestEvent: ERROR')


record_store = MemoryStore()

eventy_app = EventyApp(
    record_store=record_store,
    app_service=Service('test-sanic-eventy-app'),
    ext_services=[Service('test-sanic-eventy-app-ext', event_topic='test-topic')],
    agents=[EventyAgent()]
)

sanic_app = Sanic("Hello_World_App")


@sanic_app.route('/hello')
async def hello(request):
    return text(f"Hello World!")


@sanic_app.route('/send_ok_event')
async def send_ok_event(request):
    record_store.add_record(
        Event(
            protocol_version='3.0.0',
            namespace='urn:test-sanic-eventy-app-ext',
            name='TestEvent',
            version='1.0.0',
            source='urn:test-sanic-eventy-app-ext',
            data={'ok': True}
        ),
        'test-topic'
    )
    return text(f"Sent ok event")


@sanic_app.route('/send_error_event')
async def send_error_event(request):
    record_store.add_record(
        Event(
            protocol_version='3.0.0',
            namespace='urn:test-sanic-eventy-app-ext',
            name='TestEvent',
            version='1.0.0',
            source='urn:test-sanic-eventy-app-ext',
            data={'ok': False}
        ),
        'test-topic'
    )
    return text("Sent error event")


@sanic_app.route('/eventy_alive')
async def eventy_alive(request):
    if not eventy_app._cancelled:
        return text(f"yes")
    else:
        return text(f"no")


async def main():
    sanic_server = await sanic_app.create_server(
        port=8000, host="0.0.0.0", return_asyncio_server=True
    )
    await sanic_server.startup()
    await sanic_server.start_serving()

    def on_eventy_done(exc=None):
        logger.info(f"EventyApp done, will close sanic server.")
        sanic_server.close()

    eventy_app.run(done_callback=on_eventy_done)
    await sanic_server.wait_closed()
    logger.info(f"Sanic server closed.")


if __name__ == "__main__":
    asyncio.run(main())
