#!/usr/bin/env bash

cd "$(dirname ${BASH_SOURCE[0]})" || exit 1


echo "start celery app..."
cd app || exit 1
celery -A tasks worker --loglevel=info &> server_output &
CELERY_APP_PID=$!

sleep 1
echo "celery app warmup..."
sleep 1

echo "run client app"
python test_client.py
echo "done"

cat server_output


echo "kill celery app..."
function cleanup() {
echo "rm server_output..."
rm server_output
echo "rm -r data..."
rm -r data
}
sleep 1
kill $CELERY_APP_PID
KILL_STATUS=$?
if [ $KILL_STATUS -eq 0 ]; then
  while kill -0 $CELERY_APP_PID; do
      echo "Not dead yet..."
      sleep 1
  done
  echo "Killed celery app"
  cleanup
else
  echo "ERROR: Celery App was not running."
  cleanup
  exit 1
fi

