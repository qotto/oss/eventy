"""Client code with no access to tasks code base."""

import logging

from eventy.integration.celery import send_task
from eventy.logging import SimpleHandler
import eventy.config.celery

eventy.config.celery.CELERY_AUTO_FORGET_ON_GET = True
logging.getLogger().setLevel('DEBUG')
logging.getLogger().addHandler(SimpleHandler(colored=True))

logger = logging.getLogger(__name__)

app_task_result = send_task(
    'tasks.app_task_add',
    kwargs={'x': 2, 'y': 3, }
).get()
logger.info(f'app_task_result={app_task_result}')

shared_task_result = send_task(
    'tasks.shared_task_add',
    kwargs={'x': 2, 'y': 3}
).get()
logger.info(f'shared_task_result={shared_task_result}')

namespaced_result = send_task(
    'namespaced.tasks.namspaced_add',
    kwargs={'x': 2, 'y': 3}
).get()
logger.info(f'namespaced_result={namespaced_result}')
