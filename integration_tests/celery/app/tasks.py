"""Remote worker"""

import logging

import eventy.config
from eventy.integration.celery import Celery
from eventy.integration.celery import shared_task
from eventy.logging import SimpleHandler
from eventy.trace_id import correlation_id_var, request_id_var

eventy.config.SERVICE_NAME = 'my_service'

app = Celery('tasks')

app.config_from_object('celeryconfig')

root_logger = logging.getLogger()

root_logger.addHandler(SimpleHandler(colored=True))
root_logger.setLevel(logging.DEBUG)

logger = logging.getLogger(__name__)


def namespaced(decorator, *args, **kwargs):
    def create_namespaced_task(func):
        full_name = f'namespaced.tasks.{func.__name__}'
        return decorator(*args, name=full_name, **kwargs)(func)

    return create_namespaced_task


def add(x: int, y: int) -> dict:
    logger.info('run add')
    return {
        'my_correlation_id': correlation_id_var.get(),
        'my_request_id': request_id_var.get(),
        'sum': x + y,
    }


@app.task
def app_task_add(x: int, y: int) -> dict:
    return add(x, y)


@shared_task
def shared_task_add(x: int, y: int) -> dict:
    return add(x, y)


@namespaced(shared_task)
def namspaced_add(x: int, y: int) -> dict:
    return add(x, y)
