import asyncio

from eventy.integration.aiohttp import client_session
from eventy.trace_id import correlation_id_var


async def main():
    print("Test: No correlation_id")
    async with client_session() as session:
        async with session.get('http://0.0.0.0:8080/hello') as response:
            text = await response.text()
            if text.startswith('aiohttp_web:/hello'):
                print("OK")
            else:
                print("FAILED")
                exit(1)

    print("Test: correlation_id")
    correlation_id_var.set('my_correlation_id')
    async with client_session() as session:
        async with session.get('http://0.0.0.0:8080/hello') as response:
            text = await response.text()
            if text == 'my_correlation_id':
                print("OK")
            else:
                print("FAILED")
                exit(1)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
