#!/usr/bin/env bash

cd "$(dirname ${BASH_SOURCE[0]})" || exit 1

function blocking_kill() {
  PID="$1"
  NAME="$2"
  echo
  echo "blocking_kill> Will kill $NAME with PID=$PID."
  kill $PID
  KILL_STATUS=$?
  if [ $KILL_STATUS -eq 0 ]; then
    while kill -0 $PID; do
        echo "blocking_kill> $NAME not dead yet..."
        sleep 1
    done
    echo "blocking_kill> Killed $NAME."
    echo
  else
    echo "blocking_kill> ERROR: Could not kill $NAME, was not running."
    echo
    exit 1
  fi
}

echo "> Start http server"
python http_server 2>&1 >(tee tmp_server_output) &
HTTP_SERVER_PID=$!

sleep 1
echo "> server warmup..."
sleep 1

echo "> Start http client"
python http_client 2>&1 >(tee tmp_client_output)
HTTP_CLIENT_STATUS=$?
echo "> http_client finished"

blocking_kill $HTTP_SERVER_PID "HTTP Server"

if [ $HTTP_CLIENT_STATUS -eq 0 ]; then
  echo "> OK"
else
  echo "> FAILED"
  exit 1
fi
