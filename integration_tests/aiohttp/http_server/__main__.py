"""
Minimal HTTP server to check that x-correlation-id is present
"""
import logging

import aiohttp.web

import eventy.config
from eventy.integration.aiohttp import application
from eventy.logging import SimpleHandler
from eventy.trace_id import correlation_id_var

# configure eventy
eventy.config.SERVICE_NAME = 'aiohttp_web'
eventy.config.aiohttp.AIOHTTP_WEB_ACCESS_DISABLE_HEALTH_LOGGING = True

# setup logging
root_logger = logging.getLogger()
root_logger.addHandler(SimpleHandler(colored=True))

# setup aiohttp app
app = application()


async def health(request):
    return aiohttp.web.json_response(text="OK")


async def hello(request):
    return aiohttp.web.json_response(text=correlation_id_var.get())


app.add_routes(
    [
        aiohttp.web.get('/health', health),
        aiohttp.web.get('/hello', hello),
    ]
)

if __name__ == '__main__':
    # run aiohttp web app
    aiohttp.web.run_app(app)
