import logging
import signal
import sys
from time import sleep

from eventy.integration.celery import shared_task, Celery
from eventy.record import Event
from . import celery_config
from .eventy_app import record_store, eventy_app

logger = logging.getLogger(__name__)

celery_app = Celery('celery-app')
celery_app.config_from_object(celery_config)
celery_app.autodiscover_tasks()


def terminate_celery(exc=None):
    logger.error("Eventy app stopped, will terminate celery.")
    logger.info(f"Warm shutdown...")
    signal.raise_signal(signal.SIGTERM)
    sleep(30)
    logger.warning("Celery app still running, will force shutdown.")
    sys.exit(1)


eventy_app.run(
    done_callback=terminate_celery,
)


def _wait_app_process_events() -> bool:
    for tick in range(10):
        if len(record_store.get_records(unacked=True, unread=True)) == 0:
            return True
        elif eventy_app._cancelled:
            return False
        else:
            sleep(tick * tick * 0.01)
    return False


@shared_task
def hello(name: str) -> str:
    logger.info(f"Hello task with name {name}")
    return f'Hello {name}'


@shared_task
def send_ok_event() -> bool:
    record_store.add_record(
        Event(
            protocol_version='3.0.0',
            namespace='urn:test-celery-eventy-app-ext',
            name='TestEvent',
            version='1.0.0',
            source='urn:test-celery-eventy-app-ext',
            data={'ok': True}
        ),
        'test-topic'
    )
    return _wait_app_process_events()


@shared_task
def send_error_event() -> bool:
    record_store.add_record(
        Event(
            protocol_version='3.0.0',
            namespace='urn:test-celery-eventy-app-ext',
            name='TestEvent',
            version='1.0.0',
            source='urn:test-celery-eventy-app-ext',
            data={'ok': False}
        ),
        'test-topic'
    )
    return _wait_app_process_events()
