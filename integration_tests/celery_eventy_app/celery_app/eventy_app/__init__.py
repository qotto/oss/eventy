import logging

from eventy.messaging import EventyApp, Service, Agent, handle_event
from eventy.messaging.memory import MemoryStore
from eventy.record import Event

logger = logging.getLogger(__name__)


class EventyAgent(Agent):
    @handle_event(service='test-celery-eventy-app-ext', name='TestEvent')
    def handle(self, event: Event):
        if event.data.get('ok'):
            logger.info("EventyAgent: TestEvent: OK")
        else:
            logger.error("EventyAgent: TestEvent: ERROR")
            raise Exception('EventyAgent: TestEvent: ERROR')


record_store = MemoryStore()

eventy_app = EventyApp(
    record_store=record_store,
    app_service=Service('test-celery-eventy-app'),
    ext_services=[Service('test-celery-eventy-app-ext', event_topic='test-topic')],
    agents=[EventyAgent()]
)

