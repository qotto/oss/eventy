#!/usr/bin/env bash

cd "$(dirname ${BASH_SOURCE[0]})" || exit 1

echo "start celery app..."
bin/celery_app.sh &>server_output &

echo "CELERY_APP_PID: $CELERY_APP_PID"

sleep 1
echo "celery app warmup..."
sleep 1

echo "run client app"
if bin/client_ok.sh
then
    echo "client app ok"
else
    echo "client app failed"
    exit 1
fi
sleep 1
bin/client_error.sh
sleep 10 # Celery app should close
echo "done"

cat server_output

echo "kill celery app..."
function cleanup() {
  echo "rm server_output..."
  rm server_output
  echo "rm -r data..."
  rm -r data
}
sleep 1

if kill -0 $CELERY_APP_PID
then
  echo "Celery app still alive, it should not be."
  while kill -0 $CELERY_APP_PID; do
    echo "Not dead yet..."
    sleep 1
  done
  echo "Killed celery app"
  cleanup
  echo FAILED
  exit 1
else
  cleanup
  echo OK
  exit 0
fi

