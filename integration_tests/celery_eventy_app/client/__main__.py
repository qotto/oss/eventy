from __future__ import annotations

import logging
import sys

from environs import Env

from eventy.integration.celery import send_task

logger = logging.getLogger(__name__)

env = Env()

if len(sys.argv) != 2:
    raise ValueError("Expected 1 argument, got {}".format(len(sys.argv)))
elif sys.argv[1] == "ok":
    result = send_task(
        'celery_app.celery_tasks.send_ok_event', kwargs={}
    ).get()
    if not result:
        logger.error("Expected eventy app to be still running, got False")
        sys.exit(1)
    logger.info(f"Result: {result}.")
elif sys.argv[1] == "error":
    result = send_task(
        'celery_app.celery_tasks.send_error_event', kwargs={}
    ).get()
    logger.info(f"Result: {result}.")
else:
    raise ValueError(f"Unknown argument: {sys.argv[1]}")
