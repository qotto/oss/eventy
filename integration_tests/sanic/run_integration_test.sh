#!/usr/bin/env bash

cd "$(dirname ${BASH_SOURCE[0]})" || exit 1

echo "start sanic app..."
python app &
pid=$!

sleep 1
echo "sanic app warmup..."
sleep 1

echo "curl 127.0.0.1:8000/hello -H 'x-correlation-id: my_corr_id'"
RESULT=$(curl 127.0.0.1:8000/hello -H 'x-correlation-id: my_corr_id' 2>/dev/null)
echo Result was: $RESULT

echo "kill sanic app..."
sleep 1
kill $pid

if [ "$RESULT" = "my_corr_id" ] ; then
  echo OK
else
  echo FAILED
  exit 1
fi


exit 0
