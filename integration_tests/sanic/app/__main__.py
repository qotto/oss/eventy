from sanic.response import text

from eventy.integration.sanic import Sanic
from eventy.trace_id import correlation_id_var

app = Sanic("TestSanicApp")


@app.route('/hello')
async def hello(request):
    return text(correlation_id_var.get())


@app.route('/health')
async def health(request):
    return text('ok')


if __name__ == '__main__':
    app.run()
