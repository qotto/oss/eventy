"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import subprocess
from sys import exit

from app import settings
from app import views
from django.conf.urls import url
from app.eventy import eventy_agent, eventy_app, record_store
import logging

from eventy.messaging.memory import MemoryStore

logger = logging.getLogger(__name__)

urlpatterns = [
    url(r'^send_ok_event', views.get_send_ok_event_view(record_store)),
    url(r'^send_error_event', views.get_send_error_event_view(record_store)),
    url(r'^event_count', views.get_event_count_view(eventy_agent)),
]

def done_callback(*args, **kwargs):
    logger.warning(f"Stopping service.")
    pid_file = settings.BASE_DIR / 'gunicorn.pid'
    with open(pid_file, 'r') as f:
        pid = f.read().strip()
    logger.warning(f"Killing pid {pid}")
    subprocess.run(['kill', '-9', pid])



eventy_app.run(
    done_callback=done_callback
)
