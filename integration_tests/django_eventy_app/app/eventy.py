import logging

from eventy.messaging import Agent, handle_event, EventyApp, Service
from eventy.messaging.memory import MemoryStore
from eventy.record import Event

logger = logging.getLogger(__name__)


class EventyAgent(Agent):

    def __init__(self):
        super().__init__()
        self.event_count = 0
        self.seen_event_uuids = set()

    @handle_event(service='test-django-eventy-app-ext', name='TestEvent')
    def handle(self, event: Event):
        if event.uuid in self.seen_event_uuids:
            logger.warning(f"Received duplicate event: {event}")
            return
        self.seen_event_uuids.add(event.uuid)
        self.event_count += 1
        logger.info(f"Received event (count {self.event_count}): {event}")
        if event.data.get('ok'):
            logger.info("EventyAgent: TestEvent: OK")
        else:
            logger.error("EventyAgent: TestEvent: ERROR")
            raise Exception('EventyAgent: TestEvent: ERROR')


eventy_agent = EventyAgent()

record_store = MemoryStore()

eventy_app = EventyApp(
    record_store=record_store,
    app_service=Service('test-django-eventy-app'),
    ext_services=[Service('test-django-eventy-app-ext', event_topic='test-topic')],
    agents=[eventy_agent],
)
